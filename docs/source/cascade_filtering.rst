
.. role:: blue

.. raw:: html

    <style> .blue {color:#1f618d} </style>
    <style> .red {color:red} </style>

:blue:`CASCADe-filtering` API
=============================

.. toctree::

   cascade_filtering.stencil
   cascade_filtering.kernel
   cascade_filtering.filtering
   cascade_filtering.initialize
   cascade_filtering.utilities
