.. CASCADe documentation master file, created by
   sphinx-quickstart on Thu Jan 31 17:03:33 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. role:: red

.. role:: blue

.. raw:: html

    <style> .blue {color:#1f618d} </style>
    <style> .red {color:red} </style>

:blue:`CASCADe-filtering`: The :blue:`C`\alibration of tr\ :blue:`A`\nsit :blue:`S`\pectroscopy using :blue:`CA`\usal :blue:`D`\ata filtering package
=====================================================================================================================================================

This package is a sub package of the :blue:`CASCADe` package, developed within the
EC Horizons 2020 project :red:`Exoplanets A`. It contains all the functionality
to detect and flag cosmic ray hits in spectral images, and to create cleaned and
filtered spectral images, which can be used for spectral extraction.


Document version:
|version|

CASCADe filtering documentation
===============================

.. toctree::
   :maxdepth: 1

   install
   acknowledgments
   publications
   cascade_filtering

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
